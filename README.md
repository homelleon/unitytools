# UnityTools

UnityEditor specified utility showned in Tools category

- - - 
## Directories
- Removes files and directories in Asset folder
- Creates base directories
- - - 
## Packages
- Merges dependencies from prepared snippet list
- - - 
## Attributes
- Use [field: SerializeField, ReadOnlyField] to show properties in inspector but prevent its editing.
- - -
## ScriptTemplates
- Copy script templates into your AssetFolder
- After copy you should restart Unity
- After restart you can use script templates from Create->CustomScripts menu.
#### Simple templates:
- MonoBehaviour
- POCO
- Config (ScriptableObject)
#### ECS templates:
- System
- Component
- Baker
#### WebGL build templates
- Resizable with editable bg color
- Uses Better-Minimal-WebGL-Template
- Yandex SDK is added
Select BetterYandex template in 'PlayerSettings/Player/Resolution and Presentation' menu
#### Custom template keywords:
- [new] #NAMESPACE# - full namespace for your script
- #SCRIPTNAME# - name of your script
- #NOTRIM# - prevent space trimming
- #ROOTNAMESPACEBEGIN# & #ROOTNAMESPACEEND# - only root namespace

```
Use #NAMESPACE# with after "namespace" word and before brackets.  
ex.:
```
```C#
namespace #NAMESPACE#  
{  
	// your code  
}
```
```
Use #ROOTNAMESPACEBEGIN# before code and #ROOTNAMESPACEEND# after code without brackets.  
ex.:
```
```C#
#ROOTNAMESPACEBEGIN# 
	// your code  
#ROOTNAMESPACEEND#
```
- - - 
## Logger
homelleon.Tools.Logger assembly package with _ILogger_ and _ILogHandler_ interface
- _ConsoleLogger_ for Debug.Log console logging

## GizmosDrawer
Draws gizmos graphics from your Plain Old Class Object (POCO) or any method without forcing you to add this code into OnDrawGizmos method.
Just inject GizmosDrawer by your DI container or add to SerializeField.
- Draw(() => { Gizmos.DrawCube(position, size); }, DrawMode.Replace); - enter first action with gizmos
- Draw(() => { Gizmos.DrawCube(position, size); }); - enter all other actions with gizmos

See https://docs.unity3d.com/ScriptReference/Gizmos.html for available gizmos actions you can enter into the Draw method