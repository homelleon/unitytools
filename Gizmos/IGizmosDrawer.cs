using System;

namespace homelleon.Tools.Gizmos
{
	/// <summary>
	/// Gizmos drawer interface
	/// </summary>
	public interface IGizmosDrawer
	{
		/// <summary>
		/// Draws gizmos action. Use DrawMode Replace on the first gizmos.
		/// </summary>
		/// <param name="gizmosAction">action to draw</param>
		/// <param name="drawMode">Additive mode is default</param>
		void Draw(Action gizmosAction, DrawMode drawMode = DrawMode.Additive);

		/// <summary>
		/// Clears all gizmos draw actions
		/// </summary>
		void ClearDraw();

		/// <summary>
		/// Draw gizmos if object is selected. Use DrawMode Replace on the first gizmos.
		/// </summary>
		/// <param name="gizmosAction">action to draw</param>
		/// <param name="drawMode">Additive mode is default</param>
		void DrawOnSelected(Action gizmosAction, DrawMode drawMode = DrawMode.Additive);

		/// <summary>
		/// Clears gizmos actions for all selected objects
		/// </summary>
		void ClearSelected();
	}
}
