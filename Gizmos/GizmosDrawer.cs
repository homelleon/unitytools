using System;
using System.Collections.Generic;
using UnityEngine;

namespace homelleon.Tools.Gizmos
{
	/// <summary>
	/// Gizmos draw provider. Can help debuging and can be used in
	/// POCO objects. <para/>
	/// Just bind to SerializableField or Inject by DI container
	/// </summary>
	public class GizmosDrawer : MonoBehaviour, IGizmosDrawer
	{
		private readonly List<Action> _draws = new List<Action>();
		private readonly List<Action> _selected = new List<Action>();

		/// <summary>
		/// Draws gizmos action. Use DrawMode Replace on the first gizmos.
		/// </summary>
		/// <param name="gizmosAction">action to draw</param>
		/// <param name="drawMode">Additive mode is default</param>
		public void Draw(Action gizmosAction, DrawMode drawMode = DrawMode.Additive)
		{
			if (drawMode == DrawMode.Replace)
				ClearDraw();

			_draws.Add(gizmosAction);
		}

		/// <summary>
		/// Clear all gizmos draw actions
		/// </summary>
		public void ClearDraw() => _selected.Clear();

		/// <summary>
		/// Draw gizmos if object is selected. Use DrawMode Replace on the first gizmos.
		/// </summary>
		/// <param name="gizmosAction">action to draw</param>
		/// <param name="drawMode">Additive mode is default</param>
		public void DrawOnSelected(Action gizmosAction, DrawMode drawMode = DrawMode.Additive)
		{
			if (drawMode == DrawMode.Replace)
				ClearDraw();

			_selected.Add(gizmosAction);
		}

		/// <summary>
		/// Clears gizmos actions for all selected objects
		/// </summary>
		public void ClearSelected()
		{
			_selected.Clear();
		}

		private void OnDrawGizmos()
		{
			foreach (var action in _draws)
				action.Invoke();
		}

		private void OnDrawGizmosSelected()
		{
			foreach (var action in _selected)
				action.Invoke();
		}
	}
}
