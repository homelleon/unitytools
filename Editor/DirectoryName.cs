namespace homelleon.Tools.Editors
{
    public enum DirectoryName
    {
        Scenes,
        Scripts,
        Images,
        Prefabs,
        Materials,
        Models,
        ThirdPartyAssets
    }

    public enum ScriptDirectoryName
    {
        Implementation
    }
}
