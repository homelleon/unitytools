using System.Threading.Tasks;
using UnityEditor.PackageManager.Requests;
using UnityEditor.PackageManager;
using UnityEngine;

namespace homelleon.Tools.Editors
{
    public static class RequestUtil
    {
        public static async Task<bool> WaitForRequest(Request request)
        {
            while (!request.IsCompleted ||
                request.Status == StatusCode.InProgress)
                await Task.Yield();

            if (request.Status == StatusCode.Failure)
            {
                Debug.Log("Editor: Failed requests: " + request.Error.message);
                return false;
            }
            else
                return true;
        }
    }
}
