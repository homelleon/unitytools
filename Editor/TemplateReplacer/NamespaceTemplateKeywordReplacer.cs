using UnityEditor;
using System.IO;
using System;

namespace homelleon.Tools.Editor.TemplateReplacer
{
    public class NamespaceTemplateKeywordReplacer : AssetModificationProcessor
    {
        private const string CS_EXTENSION = ".cs";
        private const string NAMESPACE_SEPARATOR = ".";
        private const string NAMESPACE_TAG = "#NAMESPACE#";
        public static void OnWillCreateAsset(string metaFilePath)
        {
            var fileName = Path.GetFileNameWithoutExtension(metaFilePath);

            if (!fileName.EndsWith(CS_EXTENSION))
                return;

            var finalNamespace = GetFinalNamespace(metaFilePath);
            var actualFile = $"{Path.GetDirectoryName(metaFilePath)}\\{fileName}";

            var content = File.ReadAllText(actualFile);
            var newContent = content.Replace(NAMESPACE_TAG, finalNamespace);

            if (content != newContent)
            {
                File.WriteAllText(actualFile, newContent);
                AssetDatabase.Refresh();
            }
        }

        private static string GetFinalNamespace(string metaFilePath)
        {
            var segmentedPath = $"{Path.GetDirectoryName(metaFilePath)}".Split(new[] { '\\' }, StringSplitOptions.None);

            // In case of placing the class at the root of a folder such as (Editor, Scripts, etc...)  
            if (segmentedPath.Length <= 2)
                return EditorSettings.projectGenerationRootNamespace;
            else
            {
                var generatedNamespace = string.Empty;
                // Skipping the Assets folder and a single subfolder (i.e. Scripts, Editor, Plugins, etc...)
                for (var i = 2; i < segmentedPath.Length; i++)
                {
                    generatedNamespace +=
                        i == segmentedPath.Length - 1
                            ? segmentedPath[i]
                            : segmentedPath[i] + NAMESPACE_SEPARATOR; // Don't add '.' at the end of the namespace
                }

                return EditorSettings.projectGenerationRootNamespace + NAMESPACE_SEPARATOR + generatedNamespace;
            }
        }
    }
}
