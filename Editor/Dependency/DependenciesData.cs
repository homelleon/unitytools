using System;
using UnityEngine;

namespace homelleon.Tools.Editors.Dependency
{
    [Serializable]
    public struct DependenciesData
    {
        public DependencyPair[] AddDependencies;
        public DependencyPair[] RemoveDependencies;
    }

    [Serializable]
    public struct DependencyPair
    {
        public string name;
        public string url;
    }
}
