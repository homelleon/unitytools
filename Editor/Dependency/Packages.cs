using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using UnityEditor.PackageManager;
using UnityEngine;

namespace homelleon.Tools.Editors.Dependency
{
    public static class Packages
    {
        private static readonly string _defaultUrl = string.Format(ToolsDefines.SNIPPET_PATH, ToolsDefines.USER_NAME, ToolsDefines.MANIFEST_SNIPPET_ID);

        public static async Task<DependenciesData> GetExternalDependenciesAsync()
        {
            var content = await GetSnippetContent(_defaultUrl);
            return ParseDependencies(content);
        }
        
        public static async Task<DependenciesData> GetExternalDependenciesAsync(string url)
        {
            var content = await GetSnippetContent(url);
            return ParseDependencies(content);
        }

        private static DependenciesData ParseDependencies(string content) =>
            JsonUtility.FromJson<DependenciesData>(content);

        private static async Task<string> GetSnippetContent(string url)
        {
            Debug.Log("Editor: Snippet URL: " + url);
            using var client = new HttpClient();
            var response = await client.GetAsync(url);
            return await response.Content.ReadAsStringAsync();
        }

        public static async Task MergePackageFileAsync(DependenciesData dependencies)
        {
            Debug.Log("Editor: Start dependencies remove");

            var installedPackagesList = Client.List();

            if (!await RequestUtil.WaitForRequest(installedPackagesList))
                return;

            var installedPackages = installedPackagesList.Result;

            for (var i = 0; i < dependencies.RemoveDependencies.Length; i++)
            {
                var dependency = dependencies.RemoveDependencies[i];

                if (TryGetPackageId(installedPackages, dependency, out var packageId))
                {
                    Debug.Log("Editor: Removing dependency: " + dependency.name);
                    var request = Client.Remove(packageId);

                    if (!await RequestUtil.WaitForRequest(request))
                        return;
                }
            }

            Debug.Log("Editor: Start dependencies add");

            for (var i = 0;  i < dependencies.AddDependencies.Length; i++)
            {
                var dependency = dependencies.AddDependencies[i];

                if (Contains(installedPackages, dependency)) continue;

                Debug.Log("Editor: Adding dependency: " + dependency); 
                var request = Client.Add(dependency.url);

                if (!await RequestUtil.WaitForRequest(request))
                    return;
            }

            Client.Resolve();
            Debug.Log("Dependencies merged");
        }

        private static bool Contains(PackageCollection collection, DependencyPair pair) =>
            collection.Any(info => Contains(info, pair));

        private static bool TryGetPackageId(PackageCollection collection, DependencyPair pair, out string id)
        {
            id = string.Empty;
            var foundPackage = collection.Where(info => Contains(info, pair)).FirstOrDefault();

            if (foundPackage != null)
                id = foundPackage.name;

            return foundPackage != null;
        }

        private static bool Contains(PackageInfo info, DependencyPair pair) =>
            info.packageId.Contains(pair.name) || info.packageId.Contains(pair.url);
    }
}