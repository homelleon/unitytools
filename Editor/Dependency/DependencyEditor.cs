using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UnityEditor;
using UnityEngine;

namespace homelleon.Tools.Editors.Dependency
{
    public class DependencyEditor : EditorWindow
    {
        private static bool _loaded;
        private static bool _loading;
        private bool _merging;
        private bool _merged;
        private static DependenciesData _data;
        private static bool _isCustomUrl;
        private static string _customUrl;

        private KeyValuePair<DependencyPair, bool>[] _buffer;

        public new static void Show()
        {
            var window = GetWindow(typeof(DependencyEditor));
            window.titleContent = new GUIContent("Package dependency");
            window.Show();
        }


        private static Dictionary<DependencyPair, bool> _packages = new();

        private void OnGUI()
        {
            if (!ShowPreloadAndContinue()) return;

            if (_merging)
            {
                GUILayout.Label("Merging...");
                return;
            }

            if (_merged)
                GUILayout.Label("Merged!");
            else
                ShowMergeContent();

            if (GUILayout.Button("Reset"))
                Reset();
        }

        private void Reset()
        {
            _loaded = false;
            _merged = false;
            _data = default;
            _packages.Clear();
        }

        private void ShowMergeContent()
        {
            GUILayout.BeginVertical();

            _buffer = _packages.ToArray();

            foreach (var package in _buffer)
                _packages[package.Key] = GUILayout.Toggle(package.Value, package.Key.name);

            GUILayout.EndVertical();

            if (GUILayout.Button("Apply"))
            {
                _merging = true;
                _ = StartMerging();
            }
        }
        private bool ShowPreloadAndContinue()
        {
            if (!_loading && !_loaded)
            {
                _isCustomUrl = GUILayout.Toggle(_isCustomUrl, "custom url");

                if (_isCustomUrl)
                    _customUrl = GUILayout.TextField(_customUrl);

                if (GUILayout.Button("Preload"))
                    _ = PreloadDependencies();
            }

            if (_loading)
            {
                GUILayout.Label("Loading...");
                return false;
            }

            return _loaded;
        }
        private static async Task PreloadDependencies()
        {
            _loading = true;
            try
            {
                _data = _isCustomUrl ?
                    await Packages.GetExternalDependenciesAsync(_customUrl) :
                    await Packages.GetExternalDependenciesAsync();

                foreach (var dependency in _data.AddDependencies)
                    _packages.Add(dependency, true);
                foreach (var dependency in _data.RemoveDependencies)
                    _packages.Add(dependency, false);

                _loaded = true;
            }
            catch (Exception e)
            {
                Debug.LogException(e);
            }
            finally
            {
                _loading = false;
            }
        }

        private async Task StartMerging()
        {
            var dependencies = new DependenciesData
            {
                AddDependencies = _packages.Where(pair => pair.Value).Select(pair => pair.Key).ToArray(),
                RemoveDependencies = _packages.Where(pair => !pair.Value).Select(pair => pair.Key).ToArray()
            };

            _merging = true;
            await Packages.MergePackageFileAsync(dependencies);
            _merged = true;
            _merging = false;
        }
    }
}
