using homelleon.Tools.Editors.Dependency;
using System.Threading.Tasks;
using UnityEditor;

namespace homelleon.Tools.Editors
{
    public static class ToolsMain
    {

        [MenuItem("Tools/Setup/Create Default Folders")]
        public static void CreateDeaultFolders() => Directories.Create();

        [MenuItem("Tools/Setup/Clean Project Folder")]
        public static void CleanProjectFolder() => Directories.Clean();

        [MenuItem("Tools/Setup/Edit Manifest")]
        public static void EditManifest() => DependencyEditor.Show();

        [MenuItem("Tools/Setup/Templates/Copy Simple Script Templates")]
        public static async Task CopyTemplates() => await Templates.CopySimpleToAssetFolder();

        [MenuItem("Tools/Setup/Templates/Copy ECS Script Templates")]
        public static async Task CopyECS() => await Templates.CopyECSToAssetFolder();

        [MenuItem("Tools/Setup/Templates/Copy WebGL Build Templates")]
        public static async Task CopyWebGLBuild() => await Templates.CopyWebGLBuildToAssetFolder();
    }
}
