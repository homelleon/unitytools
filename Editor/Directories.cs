using System.IO;
using System;
using UnityEditor;
using UnityEngine;

namespace homelleon.Tools.Editors
{
    public static class Directories
    {
        public static void Clean()
        {
            Directory.Delete(Application.dataPath, true);
            AssetDatabase.Refresh();
        }

        public static void Create()
        {
            foreach (var value in Enum.GetValues(typeof(DirectoryName)))
                CreateFolder(value.ToString());

            foreach (var value in Enum.GetValues(typeof(ScriptDirectoryName)))
                CreateFolder(Path.Combine(DirectoryName.Scripts.ToString(), value.ToString()));

            AssetDatabase.Refresh();
        }

        private static void CreateFolder(string folderName) =>
            Directory.CreateDirectory(Path.Combine(Application.dataPath, folderName));
    }
}
