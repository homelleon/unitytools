namespace homelleon.Tools.Editors
{
    public static class ToolsDefines
    {
        public const string PACKAGE_NAME = "com.homelleon.tools";
        public const string MANIFEST_SNIPPET_ID = "3607369";
        public const string SNIPPET_PATH = "https://gitlab.com/{0}/unitytools/snippets/{1}/raw";
        public const string USER_NAME = "homelleon";
    }
}
