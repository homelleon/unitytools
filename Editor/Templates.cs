using System.IO;
using System.Threading.Tasks;

namespace homelleon.Tools.Editors
{
    public static class Templates
    {
        private const string WEBGL_BUILD_FOLDER_NAME = "WebGLTemplates";
        private const string TEMPLATES_FOLDER_NAME = "ScriptTemplates";
        private const string ECS_TEMPLATES_FOLDER_NAME = "ECS";
        private const string SIMPLE_TEMPLATES_FOLDER_NAME = "Simple";
        public async static Task CopySimpleToAssetFolder() => await FilesUtil.CopyAllToAssetFolder(Path.Combine(TEMPLATES_FOLDER_NAME, SIMPLE_TEMPLATES_FOLDER_NAME), TEMPLATES_FOLDER_NAME);
        public async static Task CopyECSToAssetFolder() => await FilesUtil.CopyAllToAssetFolder(Path.Combine(TEMPLATES_FOLDER_NAME, ECS_TEMPLATES_FOLDER_NAME), TEMPLATES_FOLDER_NAME);
        public async static Task CopyWebGLBuildToAssetFolder() => await FilesUtil.CopyAllToAssetFolder(WEBGL_BUILD_FOLDER_NAME, WEBGL_BUILD_FOLDER_NAME);
    }
}
