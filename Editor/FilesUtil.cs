using System.IO;
using UnityEditor.PackageManager;
using UnityEditor;
using System.Threading.Tasks;
using System.Linq;
using UnityEngine;

namespace homelleon.Tools.Editors
{
    public static class FilesUtil
	{
        public static async Task<UnityEditor.PackageManager.PackageInfo> GetPackageAsync()
        {
            var installedPackagesList = Client.List();

            if (!await RequestUtil.WaitForRequest(installedPackagesList))
                return null;

            return installedPackagesList.Result.FirstOrDefault(collection => collection.name.Equals(ToolsDefines.PACKAGE_NAME));
        }

        public async static Task CopyAllToAssetFolder(string sourceCategoryName, string targetCategoryName)
        {
            var toolsPackage = await GetPackageAsync();

            if (toolsPackage == null)
            {
                Debug.LogError("homelleon.Tools package is not installed!");
                return;
            }

            var sourcePath = Path.Combine(toolsPackage.assetPath, sourceCategoryName);
            var targetPath = Path.Combine(Application.dataPath, targetCategoryName);
            CopyAll(sourcePath, targetPath);
        }

        public async static Task CopyToAssetFolder(string sourceFileName, string targetCategoryName)
        {
            var toolsPackage = await GetPackageAsync();

            if (toolsPackage == null)
            {
                Debug.LogError("homelleon.Tools package is not installed!");
                return;
            }

            var sourcePath = Path.Combine(toolsPackage.assetPath, sourceFileName);
            var targetPath = Path.Combine(Application.dataPath, targetCategoryName);
            Copy(sourcePath, targetPath);
        }

        private static void Copy(string sourceFile, string targetPath)
        {
            Debug.Log("source: " + sourceFile);
            Debug.Log("target: " + targetPath);

            var targetFileName = Path.Combine(targetPath, Path.GetFileName(sourceFile));

            if (Directory.Exists(targetPath))
                File.Copy(sourceFile, targetFileName, true);
            else
            {
                Directory.CreateDirectory(targetPath);
                File.Copy(sourceFile, targetFileName, true);
            }
        }

        public static void CopyAll(string sourcePath, string targetPath)
        {
            Debug.Log("source: " + sourcePath);
            Debug.Log("target: " + targetPath);

            if (Directory.Exists(targetPath))
            {
                foreach (var rawDir in Directory.GetDirectories(sourcePath, "*", SearchOption.AllDirectories))
                    Directory.CreateDirectory(rawDir.Replace(sourcePath, targetPath));

                foreach (string newPath in Directory.GetFiles(sourcePath, "*.*", SearchOption.AllDirectories))
                    File.Copy(newPath, newPath.Replace(sourcePath, targetPath), true);
            }
            else
                FileUtil.CopyFileOrDirectory(sourcePath, targetPath);

            AssetDatabase.Refresh();
        }
    }
}
