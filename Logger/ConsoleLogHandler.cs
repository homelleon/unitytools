using System;
using UnityEngine;

namespace homelleon.Tools.Logger
{
	public class ConsoleLogHandler : ILogHandler
	{ 
		private readonly ILogger _logger;
		public ConsoleLogHandler(ILogger logger) 
		{
			_logger = logger;
		}
		public void LogException(Exception exception, UnityEngine.Object context) =>
			_logger.LogException(exception, context);

		public void LogFormat(LogType logType, UnityEngine.Object context, string format, params object[] args) =>
			_logger.LogFormat(logType, context, format, args);
	}
}
