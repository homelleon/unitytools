using System;
using System.Text;
using UnityEngine;

namespace homelleon.Tools.Logger
{
	/// <summary>
	/// WIP Console logger alike Console.Log
	/// Can be used for initial logging
	/// </summary>
	public class ConsoleLogger : ILogger
	{
		private const string ERROR_TYPE = "Error";
		private const string WARNING_TYPE = "Warning";
		private const string EXCEPTION_TYPE = "Exception";
		private const string TYPE_SEPARATOR = ": ";
		private readonly string TAG_FORMAT = $"[{{0}}] ";

		public ConsoleLogger() 
		{ 
			logHandler = new ConsoleLogHandler(this);
		}

		public ILogHandler logHandler { get; set; }
		public bool logEnabled { get; set ; }
		public LogType filterLogType 
		{ 
			get => _filteredType;
			set 
			{
				_isTypeFiltered = true;
				_filteredType = value;
			}
		}

		private bool _isTypeFiltered;
		private LogType _filteredType;

		public bool IsLogTypeAllowed(LogType logType)
		{
			throw new NotImplementedException();
		}

		#region LogType
		public void Log(LogType logType, object message) =>
			Log(logType, null, message);

		public void Log(LogType logType, string tag, object message) =>
			Log(logType, tag, message, null);

		public void Log(LogType logType, object message, UnityEngine.Object context) =>
			Log(logType, null, message, context);

		public void Log(LogType logType, string tag, object message, UnityEngine.Object context)
		{
			if (!logEnabled || IsSkippedType(logType)) return;

			var sb = new StringBuilder();

			if (!string.IsNullOrEmpty(tag))
				sb.Append(string.Format(TAG_FORMAT, tag));

			switch (logType)
			{
				case LogType.Log:
					break;
				case LogType.Error:
					sb.Append(ERROR_TYPE);
					sb.Append(TYPE_SEPARATOR);
					break;
				case LogType.Warning:
					sb.Append(WARNING_TYPE);
					sb.Append(TYPE_SEPARATOR);
					break;
				case LogType.Exception:
					sb.Append(EXCEPTION_TYPE);
					sb.Append(TYPE_SEPARATOR);
					break;
			}

			sb.Append(message);
			LogInternal(sb.ToString());
		}

		private void LogInternal(string log) => Debug.Log(log);

		private bool IsSkippedType(LogType logType) => _isTypeFiltered && logType != _filteredType;
		#endregion

		#region Log
		public void Log(object message) =>
			Log(null, message);

		public void Log(string tag, object message) =>
			Log(tag, message, null);

		public void Log(string tag, object message, UnityEngine.Object context) =>
			Log(LogType.Log, tag, message, context);
		#endregion

		#region Warning
		public void LogWarning(string tag, object message) =>
			LogWarning(tag, message, null);

		public void LogWarning(string tag, object message, UnityEngine.Object context)
		{
			if (!logEnabled) return;

			LogWarning(tag, message);
		}
		#endregion

		#region Error
		public void LogError(string tag, object message) =>
			LogError(tag, message, null);

		public void LogError(string tag, object message, UnityEngine.Object context) =>
			Log(LogType.Error, tag, message, context);
		#endregion

		#region Exception
		public void LogException(Exception exception) =>
			LogException(exception, null);

		public void LogException(Exception exception, UnityEngine.Object context) =>
			Log(LogType.Exception, null, exception.Message, context);
		#endregion

		#region Format
		public void LogFormat(LogType logType, string format, params object[] args) =>
			LogFormat(logType, null, format, args);

		public void LogFormat(LogType logType, UnityEngine.Object context, string format, params object[] args) =>
			Log(LogType.Log, null, string.Format(format, args), null);

		#endregion
	}
}
