# v0.5.0
- removed ServiceLocator to separate project: git@gitlab.com:homelleon/servicelocator.git
# v0.4.3
- added small changes for codestyle 
# v0.4.2
- fixed Register method from SceneContext and ProjectContext not unregister interface
# v0.4.1
- added Register method in ScenceContext and ProjectContext that auto-unregister services on context destroy
- added new "Unregister(Type type)" method in Services 
# v0.4.0
- added ServiceLocator framework
- added FilesUtil module for internal usage
# v0.3.2
- fixed logger tag conversion
# v0.3.1
- added ReadOnlyField attribute